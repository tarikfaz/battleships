package org.scrum.psd.battleship.controller;

import org.scrum.psd.battleship.controller.dto.Color;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.*;

public class GameController {

    public static int[] myShipHits = new int[5];
    public static int[] enemyShipHits = new int[5];

    public static boolean isValidPosition(Position position) {
        Letter column;
        try {
            column = position.getColumn();
        } catch (IllegalArgumentException e) {
            return false;
        }
        if (column != Letter.A
                && column != Letter.B
                && column != Letter.C
                && column != Letter.D
                && column != Letter.E
                && column != Letter.F
                && column != Letter.G
                && column != Letter.H
        ) {
            return false;
        }

        int row = position.getRow();
        if (row <= 0 || row > 8) {
            return false;
        }

        return true;
    }

    public static boolean checkIsHit(Collection<Ship> ships, Position shot) {
        if (ships == null) {
            throw new IllegalArgumentException("ships is null");
        }

        if (shot == null) {
            throw new IllegalArgumentException("shot is null");
        }

        for (Ship ship : ships) {
            for (Position position : ship.getPositions()) {
                if (position.equals(shot)) {

                    return true;
                }
            }
        }

        return false;
    }

    public static boolean checkIsHit(Collection<Ship> ships, Position shot, boolean isEnemy) {
        if (ships == null) {
            throw new IllegalArgumentException("ships is null");
        }

        if (shot == null) {
            throw new IllegalArgumentException("shot is null");
        }

        for (Ship ship : ships) {
            for (Position position : ship.getPositions()) {
                if (position.equals(shot)) {
                    if (isEnemy) {
                        hitEnemyShip(ship);
                    }
                    return true;
                }
            }
        }

        return false;
    }

    private static void hitEnemyShip(Ship ship) {
        switch (ship.getName()) {
            case "Aircraft Carrier":
                ++enemyShipHits[0];
                if (enemyShipHits[0] == 5) {
                    System.out.println("\u001B[32m"+"******************************************");
                    System.out.println("\u001B[32m"+"\033[0;1m" + "You have destroyed an Aircraft Carrier!");
                    System.out.println("\u001B[32m"+"******************************************");
                    printLeftShips();
                }
                break;
            case "Battleship":
                ++enemyShipHits[1];
                if (enemyShipHits[1] == 4) {
                    System.out.println("\u001B[32m"+"******************************************");
                    System.out.println("\u001B[32m"+"\033[0;1m" + "You have destroyed a Battleship!");
                    System.out.println("\u001B[32m"+"******************************************");
                    printLeftShips();
                }
                break;
            case "Submarine":
                ++enemyShipHits[2];
                if (enemyShipHits[2] == 3) {
                    System.out.println("\u001B[32m"+"******************************************");
                    System.out.println("\u001B[32m"+"\033[0;1m" + "You have destroyed a Submarine!");
                    System.out.println("\u001B[32m"+"******************************************");
                    printLeftShips();
                }
                break;
            case "Destroyer":
                ++enemyShipHits[3];
                if (enemyShipHits[3] == 3) {
                    System.out.println("\u001B[32m"+"******************************************");
                    System.out.println("\u001B[32m"+"\033[0;1m" + "You have destroyed a Destroyer!");
                    System.out.println("\u001B[32m"+"******************************************");
                    printLeftShips();
                }
                break;
            case "Patrol Boat":
                ++enemyShipHits[4];
                if (enemyShipHits[4] == 2) {
                    System.out.println("\u001B[32m"+"******************************************");
                    System.out.println("\u001B[32m"+"\033[0;1m" + "You have destroyed a Patrol Boat!");
                    System.out.println("\u001B[32m"+"******************************************");
                    printLeftShips();
                }
                break;
        }
    }

    private static List<String> calculateLeftShips() {
        List<String> shipsLeft = new ArrayList<>();

        for(int i = 0; i < enemyShipHits.length; ++i) {
            if (i == 0 && enemyShipHits[i] != 5) {
                shipsLeft.add("Aircraft Carrier");
            } else if (i == 1 && enemyShipHits[i] != 4) {
                shipsLeft.add("Battleship");
            } else if (i == 2 && enemyShipHits[i] != 3) {
                shipsLeft.add("Submarine");
            } else if (i == 3 && enemyShipHits[i] != 3) {
                shipsLeft.add("Destroyer");
            } else if (i == 4 && enemyShipHits[i] != 2) {
                shipsLeft.add("Patrol Boat");
            }
        }

        return shipsLeft;
    }

    private static void printLeftShips() {

        List<String> shipsLeft = calculateLeftShips();

        if (!shipsLeft.isEmpty()) {
            System.out.print("Ships left: ");
            for (int i = 0; i < shipsLeft.size(); ++i) {
                System.out.print(shipsLeft.get(i));
                if (i + 1 != shipsLeft.size()) {
                    System.out.print(", ");
                }
            }
            System.out.println();
        }
    }

    public static List<Ship> initializeShips() {
        return Arrays.asList(
                new Ship("Aircraft Carrier", 5, Color.CADET_BLUE),
                new Ship("Battleship", 4, Color.RED),
                new Ship("Submarine", 3, Color.CHARTREUSE),
                new Ship("Destroyer", 3, Color.YELLOW),
                new Ship("Patrol Boat", 2, Color.ORANGE));
    }

    public static boolean isShipValid(Ship ship) {
        return ship.getPositions().size() == ship.getSize();
    }

    public static Position getRandomPosition(int size) {
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(size)];
        int number = random.nextInt(size);
        Position position = new Position(letter, number);
        return position;
    }
}
