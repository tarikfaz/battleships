package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.awt.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;
    private static ColoredPrinter console;

    private static int numOfHitsMe;
    private static int numOfHitsEnemy;

    public static void main(String[] args) {
        console = new ColoredPrinter.Builder(1, false).build();

        console.setForegroundColor(Ansi.FColor.MAGENTA);
        console.println("                                     |__");
        console.println("                                     |\\/");
        console.println("                                     ---");
        console.println("                                     / | [");
        console.println("                              !      | |||");
        console.println("                            _/|     _/|-++'");
        console.println("                        +  +--|    |--|--|_ |-");
        console.println("                     { /|__|  |/\\__|  |--- |||__/");
        console.println("                    +---------------___[}-_===_.'____                 /\\");
        console.println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        console.println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        console.println("|                        Welcome to Battleship                         BB-61/");
        console.println(" \\_________________________________________________________________________|");
        console.println("");
        console.clear();

        initLists();
        InitializeGame();


        StartGame();
    }

    private  static void printInvalidPositionMessage() {
        System.out.println("Miss");
        System.out.println("\u001B[31m" + "Position is outside the playing field. Please repeat the shot.");
    }

    private static void showAnimation(String animationLink) {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(new URI(animationLink));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        console.print("\033[2J\033[;H");
        console.println("                  __");
        console.println("                 /  \\");
        console.println("           .-.  |    |");
        console.println("   *    _.-'  \\  \\__/");
        console.println("    \\.-'       \\");
        console.println("   /          _/");
        console.println("  |      _  /\" \"");
        console.println("  |     /_\'");
        console.println("   \\    \\_/");
        console.println("    \" \"\" \"\" \"\" \"");

        do {
            console.println("");
            console.println("Player, it's your turn");
            console.println("Enter coordinates for your shot :");
            Position position = parsePosition(scanner.next());

            if (position == null) {
                printInvalidPositionMessage();
                continue;
            }

            boolean isValidPosition = GameController.isValidPosition(position);
            if (!isValidPosition) {
                printInvalidPositionMessage();
                continue;
            }

            boolean isHit = GameController.checkIsHit(enemyFleet, position, true);
            if (isHit) {
                beep();

                ++numOfHitsEnemy;


                console.println("                \\         .  ./");
                console.println("              \\      .:\" \";'.:..\" \"   /");
                console.println("                  (M^^.^~~:.'\" \").");
                console.println("            -   (/  .    . . \\ \\)  -");
                console.println("               ((| :. ~ ^  :. .|))");
                console.println("            -   (\\- |  \\ /  |  /)  -");
                console.println("                 -\\  \\     /  /-");
                console.println("                   \\  \\   /  /");

                if (numOfHitsEnemy == 17) {
                    System.out.println("\u001B[32m"+"You are the winner!");
                    showAnimation("https://i.pinimg.com/originals/7e/15/63/7e1563ad5f6ebf7c3b6b51150eb03bfb.gif");
                    break;
                }

            }

            console.println(isHit ? "\u001B[32m" + "Yeah ! Nice hit !" : "\u001B[31m" + "Miss");

            position = getRandomPosition();
            isHit = GameController.checkIsHit(myFleet, position, false);
            console.println("");
            console.println(String.format("\u001B[35m" + "Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), isHit ? "hit your ship !" : "miss"));
            if (isHit) {
                beep();

                ++numOfHitsMe;

                console.println("                \\         .  ./");
                console.println("              \\      .:\" \";'.:..\" \"   /");
                console.println("                  (M^^.^~~:.'\" \").");
                console.println("            -   (/  .    . . \\ \\)  -");
                console.println("               ((| :. ~ ^  :. .|))");
                console.println("            -   (\\- |  \\ /  |  /)  -");
                console.println("                 -\\  \\     /  /-");
                console.println("                   \\  \\   /  /");

                if (numOfHitsMe == 17) {
                    System.out.println("\u001B[31m"+"You have lost!");
                    showAnimation("https://gif-finder.com/wp-content/uploads/2015/08/Minion-Crying.gif");
                    break;
                }

            }
        } while (true);
    }

    private static void beep() {
        console.print("\007");
    }

    protected static Position parsePosition(String input) {

        if (input.length() != 2) {
            return null;
        }

        String s = input.toUpperCase().substring(0, 1);
        if (!s.equals("A")
                && !s.equals("B")
                && !s.equals("C")
                && !s.equals("D")
                && !s.equals("E")
                && !s.equals("F")
                && !s.equals("G")
                && !s.equals("H")) {
            return null;
        }
        Letter letter = Letter.valueOf(s);
        try {
            int number = Integer.parseInt(input.substring(1));
            return new Position(letter, number);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static Position getRandomPosition() {
        int rows = 8;
        int lines = 8;
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(lines)];
        int number = random.nextInt(rows);
        Position position = new Position(letter, number);
        return position;
    }

    private static void InitializeGame() {
        InitializeMyFleet();

        InitializeEnemyFleet();
    }

    static List<List<String>> shipList = new ArrayList<>();
    static List<String> aircraftCarrierPositions = new ArrayList<String>();
    static List<String> battleShipPositions = new ArrayList<String>();
    static List<String> submarinePositions = new ArrayList<String>();
    static List<String> destroyerPositions = new ArrayList<String>();
    static List<String> patrolBoatPositions = new ArrayList<String>();

    private static void initLists() {
        aircraftCarrierPositions.add("A1");
        aircraftCarrierPositions.add("A2");
        aircraftCarrierPositions.add("A3");
        aircraftCarrierPositions.add("A4");
        aircraftCarrierPositions.add("A5");

        battleShipPositions.add("B1");
        battleShipPositions.add("B2");
        battleShipPositions.add("B3");
        battleShipPositions.add("B4");

        submarinePositions.add("C1");
        submarinePositions.add("C2");
        submarinePositions.add("C3");

        destroyerPositions.add("D1");
        destroyerPositions.add("D2");
        destroyerPositions.add("D3");

        patrolBoatPositions.add("E1");
        patrolBoatPositions.add("E2");

        shipList.add(aircraftCarrierPositions);
        shipList.add(battleShipPositions);
        shipList.add(submarinePositions);
        shipList.add(destroyerPositions);
        shipList.add(patrolBoatPositions);
    }

    private static void InitializeMyFleet() {
        Scanner scanner = new Scanner(System.in);
        myFleet = GameController.initializeShips();

        console.println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        for (int j = 0; j < myFleet.size(); ++j) {
            console.println("");
            console.println(String.format("Please enter the positions for the %s (size: %s)", myFleet.get(j).getName(), myFleet.get(j).getSize()));
            for (int i = 1; i <= myFleet.get(j).getSize(); i++) {
                console.print(String.format("Enter position %s of %s (i.e A3):", i, myFleet.get(j).getSize()));


//                String positionInput = scanner.next();
//                myFleet.get(j).addPosition(positionInput);

                // Uncomment for test positioning
                List<String> shipPositions = shipList.get(j);
                myFleet.get(j).addPosition(shipPositions.get(i-1));
                System.out.println(shipPositions.get(i-1));

            }
        }
    }

    private static void InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 7));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 5));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 6));
    }
}
